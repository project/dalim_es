(function ($, Drupal) {
    Drupal.behaviors.dalim_es = {
      attach: function (context) {
        var dataArray = [];
        var api = document.querySelector("#dalim_es_api").value;
        $('.dalim_thumbnail').once('dalim_es').click(function () {
            //$(this).addClass('checked');
            var itemID = $(this).attr('id');
            if(itemID != null) {
                var idx = itemID.indexOf("_");
                var suffix = itemID.substring(idx + 1);
            }

            modalEle = document.querySelector("#modal");
            modalImage = document.querySelector("#modal_img");

            var urlImage = document.querySelector("#img_" + suffix).src
            modalImage.style.backgroundImage = "url('" + urlImage + "')";

            // Retrieve the ID of the asset
            var src = urlImage.split('&esToken=')[0];
            src = src.split(':');
            const objectId = src[src.length - 2];

            // Fill the modal with the metadata
            displayModal(suffix, objectId);

            modalEle.style.display = "flex";

            // Close button
            document.querySelector(".dalim_modal_cancel").addEventListener("click", event => {
                // var iframe = document.querySelector("body");
                // iframe.classList.remove("_stopScroll");
                modalEle.style.display = "none";
            });

            //Select the previous/next buttons
            Array.from(document.querySelectorAll(".dalim_navig_button")).forEach(item => {
                //Add on click event
                item.addEventListener("click", event => {
                    var itemObjectID = item.value;
                    if(itemObjectID != '') {
                    suffix = item.name;
                        var metadatas;

                        modalEle = document.querySelector("#modal");
                        modalImage = document.querySelector("#modal_img");

                        var urlImage = document.querySelector("#img_" + suffix).src;
                        modalImage.style.backgroundImage = "url('" + urlImage + "')";;

                        // Retrieve the ID of the asset
                        var src = urlImage.split('&esToken=')[0];
                        src = src.split(':');
                        const objectId = src[src.length - 2];
                        //Replace the modal field with the metadata
                        displayModal(suffix, objectId);
                }
                });
            });

            function displayModal(suffix, objectId) {
                //Retrieve usefull data
                imageName = document.querySelector("#imgName_" + suffix).innerHTML;
                type = document.querySelector("#imgType_" + suffix).innerHTML;
                description = document.querySelector("#img_description_" + suffix).innerHTML;
                createdBy = document.querySelector("#img_createdBy_" + suffix).innerHTML;
                previousAsset = document.querySelector("#img_previousAsset_" + suffix).innerHTML;
                nextAsset = document.querySelector("#img_nextAsset_" + suffix).innerHTML;

                //Reset the URL for the copy URL method
                document.querySelector("#dalim_url_asset").value = '';

                //Select that contains the download options
                select = document.querySelector("#dalim_download");
                dalim_link = document.querySelector("#dalim_link");

                const valueNode = document.querySelector(".modal_info_box");
                //Clear it for the new datas
                valueNode.textContent = '';
                dataArray.length = 0;

                //Array that will contains metatadas
                dataArray.push([Drupal.t('Name'), imageName, imageName]);
                if (type != ''){
                    dataArray.push([Drupal.t('Type'), type, type]);
                }
                dataArray.push([Drupal.t('Description'), description, description]);
                dataArray.push([Drupal.t('Created by'), createdBy, createdBy]);

                var option = new Option(Drupal.t('Link to DALIM ES (Dynamic update)'), 'no_dl', true, true);
                var option_ori = new Option(Drupal.t('Download Original in Library'), 'original', true, false);
                var option_dl = new Option(Drupal.t('Download Thumbnail in Library'), 'thumbnail', true, false);

                if (api === 'dalim_es_es_api'){
                    //Add option to the select
                    select[0] = option_ori;
                    select[2] = option;

                    //Request the metadata

                    $.ajaxSetup({async:false});
                    $.get('/dalim_es/get/metadatas/' + objectId, null, addMetadatas);
                    $.ajaxSetup({async:true});
                }

                //Add the metadatas in the image modal window
                dataArray.forEach(element => {
                    //Create a node that will contains one data
                    var childNodeTitle = document.createElement("div");
                    var childNodeValue = document.createElement("div");
                    //Give it a class
                    childNodeTitle.classList = "miniTitle";
                    childNodeValue.classList = "miniValue";
                    childNodeTitle.textContent = element[0];
                    if (element[1] == null || element[1] == ''){
                        childNodeValue.textContent = "--";
                    }
                    else {
                        childNodeValue.textContent = element[2];
                    }
                    valueNode.append(childNodeTitle, childNodeValue);
                });

                // Show the option to add an hyperlink if it's not a video
                if (type.startsWith('image') && api === 'dalim_es_es_api'){
                    dalim_link.style.display = 'block';
                    document.querySelector("#label_link").style.display = 'block';

                } else {
                    dalim_link.style.display = 'none';
                    document.querySelector("#label_link").style.display = 'none';
                }

                    //If the document has the wrong type, show a message
                if (type.startsWith('model')){
                    select[2] = null;
                    // document.querySelector("#error_type").style.display = 'block';
                } else {
                    // document.querySelector("#error_type").style.display = 'none';
                }

                if (type.endsWith('tiff') || type.endsWith('tif')){
                    select[2] = null;
                    select[1] = null;
                    select[0] = option_dl;
                }

                //Add data to the previous/next buttons
                document.querySelector("#dalim_previous").value = previousAsset;
                document.querySelector("#dalim_next").value = nextAsset;
                document.querySelector("#dalim_previous").name = document.querySelector("#img_previousId_" + suffix).innerHTML;
                document.querySelector("#dalim_next").name = document.querySelector("#img_nextId_" + suffix).innerHTML;

                if (previousAsset == '') {
                    document.querySelector("#dalim_previous").classList.add("dalim_changed_null");
                } else {
                    document.querySelector("#dalim_previous").classList.remove("dalim_changed_null");

                }
                if (nextAsset == '') {
                    document.querySelector("#dalim_next").classList.add("dalim_changed_null");
                } else {
                    document.querySelector("#dalim_next").classList.remove("dalim_changed_null");
                }
                //Form informations
                document.querySelector("#dalim_url_picture").value = document.querySelector("#img_" + suffix).src;
                document.querySelector("#dalim_name_picture").value = imageName;
                document.querySelector("#dalim_type_picture").value = type;
                document.querySelector("#dalim_cmisId_picture").value = objectId;

                // Display the option to choose between donwloading the original or the thumbnail
                if (type.startsWith('image') || type.startsWith('application') || type.startsWith('model')){
                    select[1] = option_dl;
                } else {
                    select[1] = null;
                }

                if (type.endsWith('gif')){
                    select[1] = null;
                }

                changeButton()

                $('#modal_close').removeClass('button');
                $('#modal_close').removeClass('js-form-submit');
                $('#modal_close').removeClass('form-submit');

                $('#dalim_copy_url').removeClass('button');
                $('#dalim_copy_url').removeClass('js-form-submit');
                $('#dalim_copy_url').removeClass('form-submit');

                $('#dalim_upload').removeClass('button');
                $('#dalim_upload').removeClass('js-form-submit');
                $('#dalim_upload').removeClass('form-submit');

            }
        });

        var addMetadatas = function (response) {
            metadatas = response.data;

            //Metadata
            var width = Math.round(metadatas['width']);
            var height = Math.round(metadatas['height']);

            var fileSize = Math.round(metadatas['fileSize'] / 1000);
            var metadataType = metadatas['mimeCategory'];
            var originalDate = new Date(metadatas['CreateDate']).toLocaleString();
            var date = new Date(metadatas['creationDate']).toLocaleString();
            var rights = metadatas['rights'];
            var status = metadatas['status'];
            var lastDate = new Date(metadatas['lastModificationDate']).toLocaleString();
            var lastUser = metadatas['lastModificationUser'];
            var currentRevision = metadatas['currentRevision'];
            var lastRevision = metadatas['lastRevision'];

            size = metadatas['fileSize'];

            if (metadataType != undefined){
                dataArray.push([Drupal.t('Type'), metadataType, metadataType]);
            }
            //Push the datas in the array to add node
            if (originalDate != 'Invalid Date'){
                dataArray.push([Drupal.t('Original creation date'), originalDate, originalDate]);
            }
            if (width > 0 ){
                dataArray.push([Drupal.t('Dimensions'), width, width + " by " + height + " pixels"]);
            } else {
                dataArray.push([Drupal.t('Dimensions'), null, width + " by " + height + " pixels"]);
            }
            dataArray.push([Drupal.t('File size'), fileSize, fileSize + " KB"]);
            if (rights != undefined){
                if (rights['x-default'] != undefined){
                    dataArray.push([Drupal.t('Rights'), rights, rights['x-default']]);
                }
            }
            // else {
            //     dataArray.push(['Rights', rights, rights]);
            // }
            dataArray.push([Drupal.t('Status'), status, status]);
            dataArray.push([Drupal.t('ES creation date'), date, date]);
            dataArray.push([Drupal.t('Last modification date'), lastDate, lastDate]);
            dataArray.push([Drupal.t('Last modification user'), lastUser, lastUser]);
            dataArray.push([Drupal.t('Current/Last revision'), currentRevision, currentRevision + ' / ' + lastRevision]);

            // Case for global search : can't retrieve the type from the "normal" way (cmis query always has type=null)
            if (document.querySelector("#dalim_type_picture").innerHTML === ""){
                //document.querySelector("#type").innerHTML += metadataType;
                document.querySelector("#dalim_type_picture").innerHTML = metadataType;
                type = metadataType;
            }

            Drupal.attachBehaviors();
        }

        $('#dalim_upload', context).once('dalim_es')
        .bind('click', function () {
            objectId = $('#dalim_cmisId_picture').attr('value');
            type = document.querySelector('#dalim_link').value;
            method = document.querySelector("#dalim_download").value;
            url = $('#dalim_url_picture').attr('value');
            assetType = $('#dalim_type_picture').attr('value');
            assetName = $('#dalim_name_picture').attr('value');
            $.get('/dalim_es/get/sharing/' + objectId, {linkType: type, method: method, url: url, name: assetName, assetType: assetType}, showSharing);
            return false;
        });

        $('#dalim_copy_url', context).once('dalim_es')
        .bind('click', function () {
            //With CMIS, copy the CMIS link asset
            if (api === 'dalim_cmis_api'){
                var splitUrl = $('#dalim_cmisId_picture').attr('value').value.split('&');
                var url = splitUrl[0] + '&' + splitUrl[2];
                document.querySelector("#dalim_url_asset").value = url;
            }
            //With ES, copy the sharing download link
            else {
                objectId = $('#dalim_cmisId_picture').attr('value');
                type = 'no_link';
                method = 'no_dl';
                url = $('#dalim_url_picture').attr('value');
                assetType = $('#dalim_type_picture').attr('value');
                assetName = $('#dalim_name_picture').attr('value');
                $.ajaxSetup({async:false});
                $.get('/dalim_es/get/sharing/' + objectId, {linkType: type, method: method, url: url, name: assetName, assetType: assetType}, addUrlToCopy);
                $.ajaxSetup({async:true});
            }
            //Select an empty input
            var copyText = document.querySelector("#dalim_url_asset");
            if (!navigator.clipboard){
                copyText.style = "display: block;";
                copyText.select();
                //Copy the content
                document.execCommand("copy");
                copyText.style = "display: none;";
                //Display a message
                document.getElementById("copied_msg").innerHTML = "Copied!";
                
                //Make the message disappear
                setTimeout(function () {
                    document.getElementById("copied_msg").innerHTML = "";
                }, 2000);
            } 
            else {
                navigator.clipboard.writeText(copyText).then(
                    function(){
                        //Display a message
                        document.getElementById("copied_msg").innerHTML = "Copied!";
                        
                        //Make the message disappear
                        setTimeout(function () {
                            document.getElementById("copied_msg").innerHTML = "";
                        }, 2000);
                    })
                  .catch(
                     function() {
                        //Display a message
                        document.getElementById("copied_msg").innerHTML = "Failed!";
                        
                        //Make the message disappear
                        setTimeout(function () {
                            document.getElementById("copied_msg").innerHTML = "";
                        }, 2000);
                  });
            }
        });

        var addUrlToCopy = function (response) {
            url = response.data['download'];
            document.querySelector("#dalim_url_asset").value = url;
        }

        var showSharing = function (response) {
            url = response.data['download'];
            hyperlink = response.data['link'];
            assetName = response.data['name'];
            type = response.data['type'];

            fieldId = localStorage.getItem("dalim_es_field_id");
            fieldName = localStorage.getItem("dalim_es_field_name").toLowerCase();
            fieldType = localStorage.getItem("dalim_es_field_type");
            var fieldTypeName = '';
            if (fieldType != ''){
                fieldTypeSplit = fieldType.split("_");
                fieldTypeSplit.forEach(element => {
                    fieldTypeName += '-' + element;
                });
            }

            document.querySelector('[data-drupal-selector="edit' + fieldTypeName + '-field-' + fieldName + '-' + fieldId + '-url"]').value = url;
            thumbnail = response.data['thumbnail'];
            document.querySelector('[data-drupal-selector="edit' + fieldTypeName + '-field-' + fieldName + '-' + fieldId + '-thumbnail"]').value = thumbnail;
            document.querySelector('#' + fieldName + '-preview-img-' + fieldId).style = 'background-image: url("' + document.querySelector('[data-drupal-selector="edit' + fieldTypeName + '-field-' + fieldName + '-' + fieldId + '-thumbnail"]').value + '")';
            document.querySelector('[data-drupal-selector="edit' + fieldTypeName + '-field-' + fieldName + '-' + fieldId + '-hyperlink"]').value = hyperlink;
            document.querySelector('[data-drupal-selector="edit' + fieldTypeName + '-field-' + fieldName + '-' + fieldId + '-alt"]').value = assetName;
            document.querySelector('[data-drupal-selector="edit' + fieldTypeName + '-field-' + fieldName + '-' + fieldId + '-type"]').value = type;

            localStorage.removeItem("dalim_es_field_name");
            localStorage.removeItem("dalim_es_field_type");
            localStorage.removeItem("dalim_es_field_id");

            $('.ui-icon-closethick').click();
        }

        $('.dalim_bt_breadcrumb', context).once('dalim_es')
        .bind('click', function () {
            id = $(this).attr('id').split("_")[2];
            esfolder = $(this).attr('value');
            esname = $('#dalim_esname_bc_' + id).attr('value');
            prevId = '0';
            $.get('/dalim_es/get/folder/' + esfolder, {esname: esname, prevId: prevId}, updateModal);
            return false;
        });

        $('.dalim_subfolder', context).once('dalim_es')
        .bind('click', function () {
            id = $(this).attr('value');
            esfolder = $('#dalim_esfolder_' + id).attr('value');
            esname = $('#dalim_esname_' + id).attr('value');
            prevId = $('#dalim_prevId_' + id).attr('value');
            $.get('/dalim_es/get/folder/' + esfolder, {esname: esname, prevId: prevId}, updateModal);
            return false;
        });

        $('.dalim_back', context).once('dalim_es')
        .bind('click', function () {
            esfolder = $('#dalim_back_esfolder').attr('value');
            esname = $('#dalim_back_esname').attr('value');
            prevId = '0';
            $.get('/dalim_es/get/folder/' + esfolder, {esname: esname, prevId: prevId}, updateModal);
            return false;
        });

        var updateModal = function (response) {
            $('div#drupal-modal').html(response.data);
            Drupal.attachBehaviors();
        }

        //Change the text of the place/download button
        document.querySelector("#dalim_download").addEventListener("change", event => {
            changeButton()
        });

        //Change the display of the place/download button
        function changeButton() {
            // Change display upload/place button
            if (document.querySelector("#dalim_download").value == 'no_dl'){
                document.querySelector("#dalim_upload").innerHTML = Drupal.t('Place media');
                document.querySelector("#dalim_upload").type = 'button';
            }else {
                document.querySelector("#dalim_upload").innerHTML = Drupal.t('Download and place media');
                document.querySelector("#dalim_upload").type = 'submit';
            }

            // Display link choice for PDF thumbnail
            if (document.querySelector("#dalim_download").value == 'thumbnail' && (type.startsWith('application' ) || type.startsWith('model')) && api === 'dalim_es_es_api'){
                dalim_link.style.display = 'block';
                document.querySelector("#label_link").style.display = 'block';
            }else if ((type.startsWith('application') || type.startsWith('model')) && api === 'dalim_es_es_api') {
                dalim_link.style.display = 'none';
                document.querySelector("#label_link").style.display = 'none';
            }
        }

        //Scroll animation
        // var prevScrollPos = window.pageYOffset;
        // window.onscroll = function (){
        //     var currentScrollPos = window.pageYOffset;
        //     //const styles = window.getComputerStyle(document.querySelector('.dalim_navigation'));
        //     if (prevScrollPos > currentScrollPos){
        //         document.querySelector('.dalim_navigation').style.top="0px"
        //     }
        //     else{
        //         if(currentScrollPos > 0){
        //             document.querySelector('.dalim_navigation').style.top="-48px";
        //         } else{
        //             document.querySelector('.dalim_navigation').style.top="0px"
        //         }
        //     }
        //     prevScrollPos = currentScrollPos;
        // }

        //Global search bar event
        $('#global_search', context).once('dalim_es')
        .bind('keypress', 'input', function (e) {
            if(e.which == 13){
                $('.dalim_overlay').css({"display": "block"});
                var val = $(this).val();
                esfolder = $('#global_search_esfolder').attr('value');
                esname = $('#global_search_esname').attr('value');
                prevId = $('#global_search_prevId').attr('value');
                $.get('/dalim_es/get/folder/' + esfolder, {esname: esname, prevId: prevId, global_search: val}, updateModal);
              }
        });

        //Search bar event
        document.getElementById('search').addEventListener('keyup', event => {
            var search = document.getElementById('search').value.toLowerCase();
            var documents = document.querySelectorAll('.dalim_thumbnail');

            Array.prototype.forEach.call(documents, function (document) {
            // We found the search terms
            var name = (document.innerHTML.split('>'));
            var result = name[5].substring(0, (name[5].length - 5)).toLowerCase();
            if (result.indexOf(search) > -1) {
                document.style.display = 'block';
            } else {
                document.style.display = 'none';
            }
            });
        });

        //Button animation
        var element = document.querySelectorAll('.dalim_bt');
        element.forEach((item) => {
            item.addEventListener("mousedown", function () {
                item.classList.remove("_active");
                void item.offsetWidth;
                item.classList.add("_active")
            },false);
        })
      },
    };

  })(jQuery, Drupal);
