(function ($, Drupal) {
    Drupal.behaviors.dalim_es = {
      attach: function (context) {
        $('.dalim_es_button_asset').click(function () {
            var id = $(this).attr('id');
            const splitId = id.split("--");
            // Store the datas for the other script
            localStorage.setItem("dalim_es_field_name", splitId[0]);
            localStorage.setItem("dalim_es_field_type", splitId[1]);
            localStorage.setItem("dalim_es_field_id", splitId[2]);
        });
      },
    };
})(jQuery, Drupal);
