CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ


INTRODUCTION
------------
The DALIM ES CMS Connector allows you to connect to your DALIM ES server from your favorite CMS Drupal, and get straight access your assets to use any image, video, PDF (or PDF thumbnail) in your website.

Keep them synchronised with your DALIM ES with the help of ES API, or use the update functionality that comes with the CMIS API if you don't have the license for the ES API.

The flexibility of the DALIM ES module allows you to express all your creativity with your assets.

 * For the description of the module visit:
   https://www.drupal.org/project/dalim_es

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/node/3306222


REQUIREMENTS
------------

This module requires the following modules:

 * [Media](https://www.drupal.org/docs/8/core/modules/media)


INSTALLATION
------------

Install the Easy DALIM ES CMS Connector module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/3306222 for further
information.


CONFIGURATION
-------------
 
 * Configure the user permissions in Administration » People » Permissions:

   - Access configuration menu

     Users with this permission will see the configuration menu to enter/modify the login connexion to the DALIM ES server.

 * Enter the login information in Administration » Configuration »
   Web services » DALIM ES CMS Connector.

Field parameters:
 * Server name: the address of your DALIM ES server
 * Admin username: the name of the DALIM ES user
 * Password: the password of the user
 * API mode: the API that will be used, either CMIS or ES API. (The ES API allow more functionalities but comes with a paid license).
 * Web client username (Optional): when using the ES API, it will restrict the rights of the people opening a DIALOGUE view link.


FAQ
---

Q: To have the plugin working, do I have to do something in DALIM ES ?

A: No, there is nothing to do in DALIM ES. You only need to have a user with the corrects access to the folders with the assets needed.

Q: Which type files are included ?

A: Most of the commons type files for images and videos are included. It's also possible to display PDF.

Q: What is the difference between the usage of the CMIS et ES API ?

A: The functionality of the CMIS API are limited. As a result that with the CMIS API the assets are always download to your Drupal Media Library so the assets are not synchronised. There are also a lot of data and metadata that cannot be displayed in the Drupal interface.

The ES API come with all the advanced functionality. The assets are shared, so any change of revision in your DALIM ES will update your asset in your website. There are also more displayed information about your assets (like the status, the number of revision ...) and access to a better search method.
