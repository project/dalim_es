<?php

namespace Drupal\dalim_es;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Config\ConfigFactory;

// Setting up constants.
define("DALIM_CMIS_API_URL", "DALIM_CMIS_API_URL");
define("DALIM_CMIS_API_SERVER", "DALIM_CMIS_API_SERVER");
define("DALIM_CMIS_API_AUTO_LOGOUT", "DALIM_CMIS_API_AUTO_LOGOUT");
define("DALIM_CMIS_API_SESSION_ID", "DALIM_CMIS_API_SESSION_ID");


/**
 * DALIM ES CMIS API service.
 *
 * @package Drupal\dalim_es
 */
class DalimEsCmisApi {
  use StringTranslationTrait;

  /**
   * DALIM ES server.
   *
   * @var string
   */
  private $url = NULL;

  /**
   * DALIM ES user.
   *
   * @var string
   */
  private $username = NULL;

  /**
   * DALIM ES password.
   *
   * @var string
   */
  private $password = NULL;

  /**
   * DALIM ES options.
   *
   * @var array
   */
  private $options = [
    DALIM_CMIS_API_SERVER => NULL,
    DALIM_CMIS_API_URL => NULL,
    DALIM_CMIS_API_SESSION_ID => "PHP",
  ];

  /**
   * PHP session.
   *
   * @var string
   */
  private $phpsession = FALSE;

  /**
   * Last header.
   *
   * @var string
   */
  private $lastheader = NULL;

  /**
   * Date of last check.
   *
   * @var string
   */
  private $lastcheck = 0;

  /**
   * Last error.
   *
   * @var string
   */
  private $lasterr = NULL;

  /**
   * Root datas.
   *
   * @var string
   */
  private $esroot = [
    "Home" => "4:0:1",
    "Global search" => "Global search",
  ];

  /**
   * HTTP client object.
   *
   * @var GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Request stack object.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $stack;

  /**
   * DalimEsApi object.
   *
   * @var Drupal\dalim_es\DalimEsApi
   */
  protected $dalimEsApi;

  /**
   * Config factory object.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('http_client'),
          $container->get('request_stack'),
          $container->get('dalim_es_api'),
          $container->get('config.factory'),
      );
  }

  /**
   * Construct a DalimEsApi object.
   *
   * @param GuzzleHttp\ClientInterface $httpClient
   *   HTTP client service.
   * @param Symfony\Component\HttpFoundation\RequestStack $stack
   *   Request stack service.
   * @param Drupal\dalim_es\DalimEsApi $dalimEsApi
   *   DALIM ES service.
   * @param Drupal\Core\Config\ConfigFactory $config
   *   Config factory service.
   */
  public function __construct(ClientInterface $httpClient, RequestStack $stack, DalimEsApi $dalimEsApi, ConfigFactory $config) {
    $this->httpClient = $httpClient;
    $this->stack = $stack;
    $this->dalimEsApi = $dalimEsApi;
    $this->config = $config->getEditable('dalim_es.settings');
  }

  /**
   * Get the configuration.
   *
   * @param array $args
   *   Arguments to configure the object.
   */
  public function dalimCmisConfiguration(array $args = []) {
    $this->options = $args + $this->options;
    if ($this->options[DALIM_CMIS_API_SESSION_ID] == "PHP") {
      $this->phpsession = TRUE;
      if (isset($_SESSION[DALIM_CMIS_API_SESSION_ID])) {
        $this->options[DALIM_CMIS_API_SESSION_ID] = $_SESSION[DALIM_CMIS_API_SESSION_ID];
      }
      else {
        $this->options[DALIM_CMIS_API_SESSION_ID] = NULL;
      }
    }
  }

  /**
   * Log in with the API.
   *
   * @param string $username
   *   Name of the user.
   * @param string $password
   *   Password of the user.
   *
   * @return array
   *   ES API Session ID
   */
  public function login($username = NULL, $password = NULL) {
    $this->username = $username;
    $this->password = $password;
    $this->options[DALIM_CMIS_API_URL] = $this->options[DALIM_CMIS_API_SERVER] . "/Esprit/browser/login?u=" . $username . "&pw=" . $password;

    $result = $this->call($this->options[DALIM_CMIS_API_URL]);

    if ($result == FALSE) {
      return FALSE;
    }
    if ($result["token"] == TRUE) {
      if (isset($result["token"])) {
        $this->options[DALIM_CMIS_API_SESSION_ID] = $result["token"];
      }
      if ($this->phpsession) {
        $_SESSION[DALIM_CMIS_API_SESSION_ID] = $this->options[DALIM_CMIS_API_SESSION_ID];
      }
      return $this->options[DALIM_CMIS_API_SESSION_ID];
    }
    else {
      $this->options[DALIM_CMIS_API_SESSION_ID] = NULL;
    }

  }

  /**
   * Check if we're logged in.
   *
   * @return array
   *   The session ID
   */
  public function isLogged() {
    if ($this->options[DALIM_CMIS_API_SESSION_ID] != NULL) {
      if (time() > $this->lastcheck + 1) {
        $this->lastcheck = time();
        $url = $_SESSION['server'] . "/Esprit/browser/4:0:1";
        $result = $this->call($url);
        if ($result == FALSE) {
          session_start();
          // Clear all the sessions.
          $_SESSION["server"] = NULL;
          $_SESSION["user"] = NULL;
          $_SESSION["password"] = NULL;
          $_SESSION["dalimapi"] = NULL;
          $_SESSION["login"] = "false";
          $_SESSION["es_object"] = NULL;
          $_SESSION["es_name"] = NULL;
          $_SESSION["es_prevId"] = NULL;
          $_SESSION['ESAPI'] = NULL;
          $_SESSION["web_user"] = NULL;
          $_SESSION['bc'] = [];
          return $this->options[DALIM_CMIS_API_SESSION_ID] = NULL;
        }
        else {
          return $this->options[DALIM_CMIS_API_SESSION_ID] != NULL;
        }
      }
    }
    return $this->options[DALIM_CMIS_API_SESSION_ID] != NULL;
  }

  /**
   * Log out of the API.
   *
   * @return bool
   *   Success or fail
   */
  public function logout() {
    // Dont't put esToken at the end of the URL
    // or else it won't loggout of the session.
    $this->options[DALIM_CMIS_API_URL] = $this->options[DALIM_CMIS_API_URL] . "/logout";
    $result = $this->call($this->options[DALIM_CMIS_API_URL]);

    if ($result == "") {
      $this->options[DALIM_CMIS_API_SESSION_ID] = NULL;
      $this->options[DALIM_CMIS_API_URL] = NULL;
      $_SESSION[DALIM_CMIS_API_SESSION_ID] = NULL;
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the last error.
   *
   * @return array
   *   The last error
   */
  public function getError() {
    if (isset($this->lasterr["error"])) {
      return $this->lasterr["message"];
    }
    elseif (isset($this->lasterr["message"])) {
      return $this->lasterr["message"];
    }
    else {
      return $this->lasterr;
    }
  }

  /**
   * Get the thumbnail of the asset.
   *
   * @return string
   *   The thumbnail
   */
  public function getThumbnail($object) {
    return $this->options[DALIM_CMIS_API_SERVER] . "/Esprit/browser/root/root?objectId=" . $object . "&streamId=P-" . $object . "&esToken=" . $this->options[DALIM_CMIS_API_SESSION_ID];
  }

  /**
   * Make request to the CMIS API.
   *
   * @param string $url
   *   The DALIM ES URL.
   *
   * @return array
   *   API response
   */
  public function call($url) {
    $headers = ["Content-Type: application/json; charset=UTF-8"];
    if ($this->options[DALIM_CMIS_API_SESSION_ID] != NULL) {
      $headers[] = "Cookie: JSESSIONID=" . $this->options[DALIM_CMIS_API_SESSION_ID];
    }

    try {
      $request = $this->httpClient->get($url, [
        'auth' => [$this->username, $this->password],
        'headers' => $headers,
      ]);
      $response = $request->getBody();
      $this->lastheader = $request->getHeaders();

    }
    catch (RequestException $e) {
      watchdog_exception('dalim_es', $e->getMessage());
    }

    $httpcode = $request->getStatusCode();

    if ($httpcode == 200) {
      return json_decode($response, TRUE);
    }
    else {
      $this->lasterr = [
        "code" => $httpcode,
        "message" => "HTTP Status " . $httpcode,
      ];
      return FALSE;
    }
  }

  /**
   * Create the DALIM ES menu.
   *
   * @param string $api_used
   *   The API chosen by the user.
   *
   * @return array
   *   Contain all the content to display.
   */
  public function displayMenu($api_used) {
    // $_SESSION['DALIM_ES_POST_ESFOLDER'] = '4:0:1';
    // $_SESSION['DALIM_ES_POST_ESNAME'] = '';
    // $_SESSION['DALIM_ES_POST_PREV_ID'] = '';
    // $_SESSION['DALIM_ES_POST_POSTNAME'] = '';
    $html = [
      'page' => [
        'navigation' => [],
        'content' => [],
        'breadcrumb' => [],
        'modal' => [],
      ],
    ];

    if (!isset($_SESSION['obj']) or !isset($_SESSION['name']) or !isset($_SESSION['bc'])) {
      session_start();
      $_SESSION['obj'] = [];
      $_SESSION['name'] = [];
      $_SESSION['bc'] = [];
    }

    // Add elements to arrays.
    if ($_SESSION['DALIM_ES_POST_PREV_ID'] != "0" && $_SESSION['DALIM_ES_POST_PREV_ID'] != 'global_search' && !in_array($_SESSION['DALIM_ES_POST_PREV_ID'], $_SESSION['obj'])) {
      array_push($_SESSION['obj'], $_SESSION['DALIM_ES_POST_PREV_ID']);
      array_push($_SESSION['name'], $_SESSION['DALIM_ES_POST_ESNAME']);
    }

    // Empty the array when going to a new main folder.
    if ($_SESSION['DALIM_ES_POST_ESFOLDER'] == "4:0:1") {
      $_SESSION['obj'] = [];
      $_SESSION['name'] = [];
      $_SESSION['bc'] = [];
    }

    // Variable used to make the "Go back" button.
    $prevId = $_SESSION['DALIM_ES_POST_PREV_ID'];
    $folderName = $_SESSION['DALIM_ES_POST_ESNAME'];
    $reloadPage = $_SESSION['DALIM_ES_POST_ESFOLDER'] == "";

    if (in_array(["Global Search" => "global_search"], $_SESSION['bc']) && $_SESSION['DALIM_ES_POST_PREV_ID'] == 'global_search') {
      $prevId = end($_SESSION['obj']);
    }

    if (($prevId == "0") && !$reloadPage) {
      $pop = array_pop($_SESSION['obj']);
      array_pop($_SESSION['name']);
      while ($pop != $_SESSION['DALIM_ES_POST_ESFOLDER']) {
        $pop = array_pop($_SESSION['obj']);
        array_pop($_SESSION['name']);
        array_pop($_SESSION['bc']);
        if ($pop == "") {
          break;
        }
      }
      $prevId = end($_SESSION['obj']);

      $folderName = end($_SESSION['name']);

      array_pop($_SESSION['bc']);
    }
    // Store data in session to keep them when refreshing the page.
    if (isset($_SESSION['DALIM_ES_POST_ESFOLDER'])) {
      $_SESSION["es_object"] = $_SESSION['DALIM_ES_POST_ESFOLDER'];
      $_SESSION["es_name"] = $folderName;
      $_SESSION["es_prevId"] = $prevId;
    }

    if (NULL != $this->stack->getCurrentRequest()->request->get("dalim_prevName")) {
      $_SESSION["es_name"] = $_SESSION['DALIM_ES_POST_ESNAME'];
      $_SESSION["es_prevId"] = $_SESSION['DALIM_ES_POST_PREV_ID'];
    }

    if ($_SESSION['DALIM_ES_POST_POSTNAME'] != NULL) {
      $_SESSION["es_object"] = "global_search";
    }

    // Call the navigation() and getEsFolder() function.
    $html = $this->navigation($_SESSION["es_name"], $_SESSION["es_object"], $_SESSION["es_prevId"], $html);
    if ($_SESSION["es_object"] == "global_search") {
      $html = $this->getGlobalSearch($html, $_SESSION['DALIM_ES_POST_POSTNAME']);
    }
    else {
      $html = $this->getEsFolder($_SESSION["es_object"], $_SESSION["es_name"], $html);
    }

    // Display the breadcrumb path.
    $prevId = "4:0:1";
    $prevName = "Home";
    $html['page']['breadcrumb'] = [
      '#prefix' => '<div class="dalim_breadcrumb_bar">',
      '#suffix' => '</div>',
    ];
    foreach ($_SESSION['bc'] as $index => $object) {
      foreach ($object as $name => $obj) {
        $html['page']['breadcrumb'][$obj] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          'content' => [
            '#type' => 'inline_template',
            '#template' => '
                            <button type="submit" name="dalim_esfolder" id="dalim_bc_{{ index }}" value="{{ obj }}" class="dalim_bt_breadcrumb">{{ name }}</button>
                            <input type="hidden" name="dalim_esname" id="dalim_esname_bc_{{ index }}" value="{{ name }}"/>
                            <input type="hidden" name="dalim_prevId" id="dalim_prevId_bc_{{ index }}" value="{{ prevId }}"/>
                            <input type="hidden" name="dalim_prevName" id="dalim_prevName_bc_{{ index }}" value="{{ prevName }}"/>
                          ',
            '#context' => [
              'index' => $index,
              'obj' => $obj,
              'name' => $name,
              'prevId' => $prevId,
              'prevName' => $prevName,
            ],
          ],
          '#attributes' => [
            'class' => ['dalim_breadcrumb'],
          ],
        ];
        // Create a button for each folders.
        $prevId = $obj;
        $prevName = $name;
      }

    }
    $html['page']['modal'] = $this->submit();
    $html['page']['api'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'dalim_es_api',
      ],
      '#value' => $api_used,
    ];
    $html['#attached']['library'][] = 'dalim_es/dalim-es';
    $_SESSION['DALIM_ES_POST_ESFOLDER'] = NULL;
    $_SESSION['DALIM_ES_POST_ESNAME'] = NULL;
    $_SESSION['DALIM_ES_POST_PREV_ID'] = NULL;
    return $html;

  }

  /**
   * Create the navigation head.
   *
   * @param string $folderName
   *   The name of the folder.
   * @param string $folderId
   *   The objectId of the asset.
   * @param string $prevId
   *   The objectId of the previous asset.
   * @param array $html
   *   The objectId of the asset.
   *
   * @return array
   *   Contain all the content to display.
   */
  public function navigation($folderName, $folderId, $prevId, array $html) {

    // Pair object ID with a name.
    if ($folderId == "4:0:1") {
      $folderName = "Home";
    }
    elseif ($folderId == "") {
      $folderId = "4:0:1";
      $folderName = "Home";
    }
    elseif ($folderId == "global_search") {
      $folderName = "Global Search";
    }

    // Add the folder in the path.
    $path = [$folderName => $folderId];
    if (!in_array($path, $_SESSION['bc'])) {
      array_push($_SESSION['bc'], $path);
    }

    $count = count($_SESSION['bc']);
    $arr = [$this->stack->getCurrentRequest()->request->get("dalim_esname") => $this->stack->getCurrentRequest()->request->get("dalim_esfolder")];
    $index = array_search($arr, array_values($_SESSION['bc']));
    $index += 1;
    // $i = 0;
    // Condition to not make the path disapear when using the "Go back" button.
    if ($index != 1) {
      array_splice($_SESSION['bc'], $index);
    }

    $html = [
      'page' => [
        'navigation' => [
          '#prefix' => '<div class="dalim_navigation">',
          '#suffix' => '</div>',
          'header' => [],
          'navig' => [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#value' => $this->t('<input type="text" id="search" placeholder="Quick filter" />'),
            '#attributes' => [
              'class' => ['dalim_search_bar'],
            ],
          ],
        ],
      ],
    ];

    if ($folderId != "") {
      // Display the folder name.
      $html['page']['navigation']['header'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => ['dalim_folder_header'],
        ],
      ];
      $html['page']['navigation']['header']['left'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => ['dalim_header_left'],
        ],
      ];

      // Display the "Go back" button.
      if ($count > 1) {
        $html['page']['navigation']['header']['left']['folder'] = [
          'dalim_gb_bt' => [
            '#type' => 'html_tag',
            '#tag' => 'button',
            '#attributes' => [
              'class' => [
                'dalim_back',
                'icon-uni7F2',
              ],
              'name' => 'dalim_gb_bt',
            ],
          ],
          'dalim_esfolder' => [
            '#type' => 'hidden',
            '#name' => 'dalim_esfolder',
            '#value' => $prevId,
            '#attributes' => [
              'id' => 'dalim_back_esfolder',
            ],
          ],
          'dalim_esname' => [
            '#type' => 'hidden',
            '#name' => 'dalim_esname',
            '#value' => $folderName,
            '#attributes' => [
              'id' => 'dalim_back_esname',
            ],

          ],
        ];
      }
      else {
        $html['page']['navigation']['header']['left']['folder'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => ['dalim_empty'],
          ],
        ];
      }

      $html['page']['navigation']['header']['left']['folderName'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $folderName,
        '#attributes' => [
          'class' => ['dalim_folder_name'],
          'title' => $folderName,
        ],
      ];

      $html['page']['navigation']['header']['search'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        'content' => [
          '#type' => 'inline_template',
          '#template' => '
                            <input type="search" name="name" id="global_search" placeholder="{{ placeholder }}" />
                            <input type="hidden" name="dalim_esfolder" id="global_search_esfolder" value="global_search"/>
                            <input type="hidden" name="dalim_esname" id="global_search_esname" value="Global search"/>
                            <input type="hidden" name="dalim_prevId" id="global_search_prevId" value="{{ folderId }}"/>          
          ',
          '#context' => [
            'folderId' => $folderId,
            'placeholder' => $this->t('Search...'),
          ],
        ],
        '#attributes' => [
          'class' => ['dalim_global_search_bar'],
        ],
      ];
      $html['page']['navigation']['header']['right'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => ['dalim_header_right_empty'],
        ],

      ];
    }
    return $html;
  }

  /**
   * Allow to navigate through the ES folder hierarchy from ES.
   *
   * @param string $objectId
   *   The objectId of the asset.
   * @param string $folderName
   *   The name of the folder.
   * @param array $html
   *   The objectId of the asset.
   *
   * @return array
   *   Contain all the content to display.
   */
  public function getEsFolder($objectId, $folderName, array $html) {

    if ($objectId == "") {
      $objectId = "4:0:1";
      $folderName = "home";
    }

    $this->options[DALIM_CMIS_API_URL] = $this->options[DALIM_CMIS_API_SERVER] . "/Esprit/browser/root/root?objectId=" . $objectId . "&esToken=" . $this->options[DALIM_CMIS_API_SESSION_ID];
    $result = $this->call($this->options[DALIM_CMIS_API_URL]);
    $objects = $result["objects"];
    $numItems = $result["numItems"];

    $html['page']['content'] = [
      '#prefix' => '<div class="dalim_folders_assets">',
      '#suffix' => '</div>',
    ];

    if ($numItems == 0) {
      // Display message when folder is empty.
    }
    else {
      // Display number of items available.
      $f = 0;
      $a = 0;
      for ($i = 0; $i < count($objects); $i++) {
        $props = $objects[$i]["object"]["properties"];
        if ($props["cmis:baseTypeId"]["value"] == "cmis:folder") {
          $f++;
        }
        elseif ($props["cmis:baseTypeId"]["value"] == "cmis:document") {
          $a++;
        }
      }

      // Display folders.
      if ($f > 0) {

        $html['page']['content']['galleryTitle'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->t('Folders <div class="dalim_assets_nbr">(@f items)</div>', ['@f' => $f]),
          '#attributes' => [
            'class' => ['dalim_gallery_title'],
          ],
        ];
        $html['page']['content']['wrapperFolders'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => ['dalim_wrapper_folder'],
          ],
        ];

        for ($i = 0; $i < count($objects); $i++) {
          $props = $objects[$i]["object"]["properties"];
          $obj = $props["cmis:objectId"]["value"];
          $name = $props["cmis:name"]["value"];
          // $objType = $props["cmis:objectTypeId"]["value"];
          if ($props["cmis:baseTypeId"]["value"] == "cmis:folder") {

            $html['page']['content']['wrapperFolders'][$obj] = [
              '#type' => 'html_tag',
              '#tag' => 'div',
            ];
            $html['page']['content']['wrapperFolders'][$obj]['content'] = [
              '#type' => 'inline_template',
              '#template' => '
                                    <button type="submit" name="dalim_esfolder" class="dalim_bt dalim_subfolder" value="{{ i }}"> 
                                        <div class="dalim_folder_image icon-folder"></div>
                                        {{ name }}
                                    </button>
                                    <input type="hidden" name="dalim_esfolder" id="dalim_esfolder_{{ i }}" value="{{ obj }}"/>
                                    <input type="hidden" name="dalim_esname" id="dalim_esname_{{ i }}"  value="{{ name }}"/>
                                    <input type="hidden" name="dalim_prevId" id="dalim_prevId_{{ i }}"  value="{{ objectId }}"/>
                            ',
              '#context' => [
                'i' => $i,
                'name' => $name,
                'obj' => $obj,
                'objectId' => $objectId,
              ],
            ];
          }
        }

      }

      // Display assets.
      if ($a > 0) {

        $html['page']['content']['galleryTitle2'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->t('Assets <div class="dalim_assets_nbr">(@a items)</div>', ['@a' => $a]),
          '#attributes' => [
            'class' => ['dalim_gallery_title'],
          ],
        ];
        $html['page']['content']['wrapperAssets'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => ['dalim_wrapper_asset'],
          ],
        ];

      }

      for ($i = 0; $i < count($objects); $i++) {
        $props = $objects[$i]["object"]["properties"];
        // Information used for the caption.
        $obj = $props["cmis:objectId"]["value"];
        $obj = substr($obj, 0, -1) . "0";
        $name = $props["cmis:name"]["value"];
        $type = $props["cmis:contentStreamMimeType"]["value"];
        $createdBy = $props["cmis:createdBy"]["value"];
        $version = $props["cmis:versionLabel"]["value"];
        if ($i > 0) {
          $previousAsset = $objects[$i - 1]["object"]["properties"]["cmis:objectId"]["value"];
        }
        else {
          $previousAsset = NULL;
        }
        if ($i < count($objects) - 1) {
          $nextAsset = $objects[$i + 1]["object"]["properties"]["cmis:objectId"]["value"];
        }
        else {
          $nextAsset = NULL;
        }

        $description = $props["cmis:description"]["value"];

        $url = $this->options[DALIM_CMIS_API_SERVER] . "/Esprit/browser/root/root?objectId=" . $obj . "&streamId=P-" . $obj;

        $html = $this->displayImage($props, $obj, $name, $type, $createdBy, $version, $description, $url, $i, $previousAsset, $nextAsset, $objects, $html);
      }
    }

    return $html;
  }

  /**
   * Get all the assets corresponding to the search bar.
   *
   * @param array $html
   *   Contain all the content to display.
   * @param string $postName
   *   Text enter in the search bar.
   *
   * @return array
   *   Contain all the content to display.
   */
  public function getGlobalSearch(array $html, $postName = NULL) {

    $html['page']['content'] = [
      '#prefix' => '<div class="dalim_folders_assets">',
      '#suffix' => '</div>',
    ];

    // A search has been made.
    if ($postName != NULL) {

      $server = $this->config->get('server_name');
      $username = $this->config->get('admin_username');
      $password = $this->config->get('admin_password');
      $api_used = $this->config->get('api_mode');

      // Different types of search for the CMIS and ES API.
      if ($api_used === 'dalim_es_cmis_api') {
        $url = $server . "/Esprit/browser/4:0:1";
        $body = [
          "cmisaction" => "query",
          "statement" =>
          "select * from cmis:document where cmis:name like '%" . $postName . "%'",
        ];

        try {
          $request = $this->httpClient->post($url, [
            'auth' => [$username, $password],
            'form_params' => $body,
          ]);
          $response = $request->getBody();
        }
        catch (RequestException $e) {
          watchdog_exception('dalim_es', $e->getMessage());
        }
        $result = json_decode($response, TRUE);

        $count = count($result['results']);

        $html['page']['content']['galleryTitle'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->t('Results <div class="dalim_assets_nbr">(@count items)</div>', ['@count' => $count]),
          '#attributes' => [
            'class' => ['dalim_gallery_title'],
          ],
        ];
        $html['page']['content']['wrapperFolders'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => ['dalim_wrapper_folder'],
          ],
        ];

        if ($result['results'] == NULL) {
          $html['page']['content']['wrapperFolders'] = [
            '#value' => $this->t('No results found ...'),
          ];
        }

        $html['page']['content']['wrapperAssets'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => ['dalim_wrapper_asset'],
          ],
        ];

        $i = 0;
        foreach ($result['results'] as $file) {
          $props = $file['properties'];

          // Information used for the caption.
          $obj = $props["cmis:objectId"]["value"];

          if ($obj == '') {
            $obj = substr($props["cmis:contentStreamId"]["value"], 2);
          }

          $obj = substr($obj, 0, -1) . "0";

          $name = $props["cmis:name"]["value"];
          $type = $props["cmis:contentStreamMimeType"]["value"];
          $createdBy = $props["cmis:createdBy"]["value"];
          $version = $props["cmis:versionLabel"]["value"];

          if ($name == '') {
            $name = $props["cmis:contentStreamFileName"]["value"];
          }

          if ($i > 0) {
            $previousAsset = $result['results'][$i - 1]["properties"]["cmis:objectId"]["value"];
          }
          else {
            $previousAsset = NULL;
          }
          if ($i < (count($result['results']) - 1)) {
            $nextAsset = $result['results'][$i + 1]["properties"]["cmis:objectId"]["value"];
          }
          else {
            $nextAsset = NULL;
          }

          $description = $props["cmis:description"]["value"];

          $url = $server . "/Esprit/browser/root/root?objectId=" . $obj . "&streamId=P-" . $obj;

          $html = $this->displayImage($props, $obj, $name, $type, $createdBy, $version, $description, $url, $i, $previousAsset, $nextAsset, NULL, $html);
          $i++;
        }
      }
      else {
        $this->dalimEsApi->dalimEsConfiguration([
          'server_name' => $server,
          'admin_username' => $username,
          'admin_password' => $password,
        ]);
        // $id = $_REQUEST["id"];
        $result = $this->dalimEsApi->getProductionSearch($postName);

        $count = count($result);

        $html['page']['content']['galleryTitle'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->t('Results <div class="dalim_assets_nbr">(@count items)</div>', ['@count' => $count]),
          '#attributes' => [
            'class' => ['dalim_gallery_title'],
          ],
        ];
        if ($result == NULL) {
          $html['page']['content']['wrapperFolders'] = [
            '#value' => $this->t('No results found ...'),
          ];
        }
        else {
          $html['page']['content']['wrapperAssets'] = [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#attributes' => [
              'class' => ['dalim_wrapper_asset'],
            ],
          ];

          $i = 0;
          foreach ($result as $file) {
            if ($file['class'] == 'PageOrder') {
              $infos = $this->dalimEsApi->getDocument($file['ID']);
              $props["cmis:baseTypeId"]["value"] = "cmis:document";
              $obj = '1:' . $infos['ID'] . ':0';
              $name = $infos['name'];
              $type = '';
              $createdBy = $infos['creationUser'];
              $version = '';

              if ($i > 0) {
                $previousAsset = '1:' . $result[$i - 1]['ID'] . ':0';
              }
              else {
                $previousAsset = NULL;
              }
              if ($i < count($result) - 1) {
                $nextAsset = '1:' . $result[$i + 1]['ID'] . ':0';
              }
              else {
                $nextAsset = NULL;
              }

              $description = $props["cmis:description"]["value"];

              $description = $infos['metadataLayout']['description'];
              $url = $server . "/Esprit/browser/root/root?objectId=" . $obj . "&streamId=P-" . $obj;
              $html = $this->displayImage($props, $obj, $name, $type, $createdBy, $version, $description, $url, $i, $previousAsset, $nextAsset, NULL, $html);
              $i++;
            }
          }
        }
      }
    }
    return $html;
  }

  /**
   * Display the thumbnail image and the modal.
   *
   * @return array
   *   Contain all the content to display.
   */
  public function submit() {
    $form['form'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['dalim_form_media'],
      ],
    ];
    $form['form']['modal'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['dalim_modal'],
        'id' => 'modal',
      ],
      'content' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => ['modal_content'],
        ],
        'image_wrapper' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => ['dalim_modal_image_wrapper'],
          ],
          'image' => [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#attributes' => [
              'class' => ['dalim_modal_image'],
              'id' => 'modal_img',
            ],
          ],
          'previous' => [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#attributes' => [
              'class' => ['dalim_navig_button'],
              'id' => 'dalim_previous',
            ],

            '#value' => '&#10094;',
          ],
          'next' => [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#attributes' => [
              'class' => ['dalim_navig_button'],
              'id' => 'dalim_next',
            ],
            '#value' => '&#10095;',
          ],
        ],
        'infos_wrapper' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => ['dalim_modal_infos_wrapper'],
          ],
          'infos_box' => [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#attributes' => [
              'class' => ['modal_info_box'],
            ],
          ],
          'option_box' => [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#attributes' => [
              'class' => ['modal_option_box'],
            ],
            'label_download' => [
              '#type' => 'html_tag',
              '#tag' => 'label',
              '#value' => $this->t('Placement Method'),
              '#attributes' => [
                'class' => ['miniTitle'],
                'for' => 'download',
              ],
            ],
            'select_download' => [
              '#type' => 'select',
              '#options' => [
                'original' => $this->t('Download Original in Library'),
              ],
              '#attributes' => [
                'class' => ['miniValue'],
                'name' => 'download',
                'id' => 'dalim_download',
              ],
            ],
            'label_link' => [
              '#type' => 'html_tag',
              '#tag' => 'label',
              '#value' => $this->t('Hyperlink'),
              '#attributes' => [
                'class' => ['miniTitle'],
                'for' => 'link',
                'id' => 'label_link',
              ],
            ],
            'select_link' => [
              '#type' => 'select',
              '#options' => [
                'no_link' => $this->t('No link'),
                'view_link' => $this->t('DIALOGUE link'),
                'dl_link' => $this->t('Download link'),
              ],
              '#attributes' => [
                'class' => ['miniValue'],
                'name' => 'link',
                'id' => 'dalim_link',
              ],
            ],
          ],
          'button_box' => [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#attributes' => [
              'class' => ['modal_button_box'],
            ],
            'cancel' => [
              '#type' => 'button',
              '#attributes' => [
                'class' => [
                  'dalim_copy_button',
                  'dalim_modal_cancel',
                ],
              ],
              '#id' => 'modal_close',
              '#name' => 'submit_close',
              '#value' => $this->t('Cancel'),
            ],
            'spacer' => [
              '#type' => 'html_tag',
              '#tag' => 'div',
              '#attributes' => [
                'class' => ['dalim_spacer'],
              ],
            ],
            'copy' => [
              '#type' => 'button',
              '#attributes' => [
                'class' => [
                  'dalim_copy_button',
                ],
              ],
              '#id' => 'dalim_copy_url',
              '#value' => $this->t('Copy URL'),
              '#name' => 'submit_copy',
            ],
            'copyText' => [
              '#type' => 'html_tag',
              '#tag' => 'div',
              '#attributes' => [
                'id' => 'copied_msg',
              ],
            ],
            'upload' => [
              '#type' => 'button',
              '#attributes' => [
                'class' => [
                  'dalim_copy_button',
                  '_highlight',
                ],
              ],
              '#id' => 'dalim_upload',
              '#value' => $this->t('Place media'),
            ],
          ],
        ],
      ],
    ];
    $form['form']['hidden'] = [
      'dalim_url_asset' => [
        '#type' => 'html_tag',
        '#tag' => 'input',
        '#attributes' => [
          'style' => 'display: none;',
          'name' => 'dalim_url_asset',
          'id' => 'dalim_url_asset',
          'type' => 'text',
        ],
      ],
      'urlpicture' => [
        '#type' => 'hidden',
        '#name' => 'urlpicture',
        '#attributes' => [
          'id' => 'dalim_url_picture',
        ],
      ],
      'namepicture' => [
        '#type' => 'hidden',
        '#name' => 'namepicture',
        '#attributes' => [
          'id' => 'dalim_name_picture',
        ],
      ],
      'typepicture' => [
        '#type' => 'hidden',
        '#name' => 'typepicture',
        '#attributes' => [
          'id' => 'dalim_type_picture',
        ],
      ],
      'cmisId' => [
        '#type' => 'hidden',
        '#name' => 'cmisId',
        '#attributes' => [
          'id' => 'dalim_cmisId_picture',
        ],
      ],
    ];
    $form['form']['popup'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['dalim_overlay'],
      ],
      'loading' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => ['dalim_popup'],
        ],
        'content' => [
          '#type' => 'html_tag',
          '#tag' => 'img',
          '#attributes' => [
            'src' => file_create_url(drupal_get_path('module', 'dalim_es') . '/images/loading.gif'),
            'class' => ['dalim_loading'],
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Display the thumbnail image and the modal.
   *
   * @return array
   *   Contain all the content to display.
   */
  public function displayImage($props, $obj, $name, $type, $createdBy, $version, $description, $url, $i, $previousAsset, $nextAsset, $objects, $form) {
    $previousId = $i - 1;
    $nextId = $i + 1;
    // In case there iss a folder between the asset,
    // jump to the prev/next asset or else the right/left arrow won't works.
    if ($objects != NULL) {
      if ($previousAsset != '') {

        $prev = $i;
        do {
          $prev--;
        } while ($objects[$prev]["object"]["properties"]["cmis:baseTypeId"]["value"] != "cmis:document" && $prev > 0);
        $previousAsset = $objects[$prev]["object"]["properties"]["cmis:objectId"]["value"];
        $previousId = $prev;
      }
      if ($nextAsset != '') {

        $next = $i;
        do {
          $next++;
        } while ($objects[$next]["object"]["properties"]["cmis:baseTypeId"]["value"] != "cmis:document" && $next < count($objects));
        $nextAsset = $objects[$next]["object"]["properties"]["cmis:objectId"]["value"];
        $nextId = $next;
      }
    }
    if ($props["cmis:baseTypeId"]["value"] == "cmis:document") {
      // Gallery thumbnail.
      $form['page']['content']['wrapperAssets'][$i] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => ['dalim_thumbnail'],
          'id' => 'thumb_' . $i,
        ],
        'image' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => ['dalim_img_wrapper'],
            'id' => 'wrapper_' . $i,
          ],
          'thumbnail' => [
            '#type' => 'html_tag',
            '#tag' => 'img',
            '#attributes' => [
              'class' => ['dalim_thumbnail_img'],
              'id' => 'img_' . $i,
              'src' => $url . "&esToken=" . $this->options[DALIM_CMIS_API_SESSION_ID],
            ],
          ],
        ],
        'infos' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => ['dalim_infos'],
            'id' => 'infos_' . $i,
          ],
          'content' => [
            '#type' => 'inline_template',
            '#template' => '
                              <div id="imgName_{{ i }}" class="dalim_title_asset">{{ name }}</div>
                              <div class="dalim_spacer"></div>
                              <div id="imgType_{{ i }}" class="dalim_type_asset">{{ type }}</div>
                              <div id="img_description_{{ i }}" hidden>{{ description }}</div>
                              <div id="img_createdBy_{{ i }}" hidden>{{ createdBy }}</div>
                              <div id="img_previousAsset_{{ i }}" hidden>{{ previousAsset }}</div>
                              <div id="img_nextAsset_{{ i }}" hidden>{{ nextAsset }}</div>
                              <div id="img_previousId_{{ i }}" hidden>{{ previousId }}</div>
                              <div id="img_nextId_{{ i }}" hidden>{{ nextId }} </div>
                          ',
            '#context' => [
              'i' => $i,
              'name' => $name,
              'type' => $type,
              'description' => $description,
              'createdBy' => $createdBy,
              'previousAsset' => $previousAsset,
              'nextAsset' => $nextAsset,
              'previousId' => $previousId,
              'nextId' => $nextId,
            ],
          ],
        ],
      ];

    }
    return $form;
  }

}
