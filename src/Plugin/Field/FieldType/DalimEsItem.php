<?php

namespace Drupal\dalim_es\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a DALIM ES field.
 *
 * @FieldType(
 *   id = "dalim_es_assets_item",
 *   label = @Translation("DALIM ES field"),
 *   description = @Translation("This field places DALIM ES assets"),
 *   category = @Translation("Reference"),
 *   default_widget = "dalim_es_assets_default_widget",
 *   default_formatter = "dalim_es_assets_default_formatter"
 * )
 */
class DalimEsItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'url' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => FALSE,
        ],
        'width' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => FALSE,
        ],
        'height' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => FALSE,
        ],
        'hyperlink' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => FALSE,
        ],
        'alt' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => FALSE,
        ],
        'type' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => FALSE,
        ],
        'thumbnail' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('url')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['url'] = DataDefinition::create('string')
      ->setLabel(t('URL of the asset'));

    $properties['width'] = DataDefinition::create('string')
      ->setLabel(t('Width of the asset'));

    $properties['height'] = DataDefinition::create('string')
      ->setLabel(t('Height of the asset'));

    $properties['hyperlink'] = DataDefinition::create('string')
      ->setLabel(t('Hyperlink'));

    $properties['alt'] = DataDefinition::create('string')
      ->setLabel(t('Alt asset'));

    $properties['type'] = DataDefinition::create('string')
      ->setLabel(t('Type'));

    $properties['thumbnail'] = DataDefinition::create('string')
      ->setLabel(t('Thumbnail'));

    return $properties;
  }

}
