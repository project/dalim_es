<?php

namespace Drupal\dalim_es\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Render\Renderer;

/**
 * Plugin implementation of the 'dalim_es_assets_default' formatter.
 *
 * @FieldFormatter(
 *   id = "dalim_es_assets_default_formatter",
 *   label = @Translation("DALIM ES assets default"),
 *   field_types = {
 *     "dalim_es_assets_item"
 *   }
 * )
 */
class DalimEsDefaultFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Renderer object.
   *
   * @var Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('renderer'),
    );
  }

  /**
   * Construct a DalimEsDefaultFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, Renderer $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      // Render output using dalim_es_assets_default theme.
      $source = [
        'image' => [
          '#theme' => 'dalim_es_assets_default',
          '#url' => file_create_url($item->url),
          '#type' => $item->type,
          '#alt' => $item->alt,
          '#href' => $item->hyperlink,
          '#width' => $item->width,
          '#height' => $item->height,
        ],
      ];
      $elements[$delta] = ['#markup' => $this->renderer->render($source)];
    }
    return $elements;
  }

}
