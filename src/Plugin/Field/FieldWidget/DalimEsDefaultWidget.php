<?php

namespace Drupal\dalim_es\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Component\Serialization\Json;

/**
 * Plugin implementation of the 'dalim_es_assets_default' widget.
 *
 * @FieldWidget(
 *   id = "dalim_es_assets_default_widget",
 *   label = @Translation("DALIM ES default"),
 *   field_types = {
 *     "dalim_es_assets_item"
 *   }
 * )
 */
class DalimEsDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dalim_es_field_form';
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $field = explode(' (value', $element['#title']);
    $link_url = Url::fromRoute('dalim_es.modal');
    $name = str_replace('_', '-', $field[0]);

    $link_url->setOptions([
      'attributes' => [
        'class' => [
          'use-ajax',
          'button',
          'button--small',
          'dalim_es_button_asset',
        ],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 1800]),
        'id' => $name . '--' . $form['#parents'][0] . '--' . $delta,
      ],
    ]);

    $element = [
      '#type' => 'details',
      '#title' => 'DALIM ES ASSET',
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['dalim_es_details'],
      ],
      '#attached' => ['library' => ['dalim_es/dalim-es-field']],
    ];
    $element['url'] = [
      '#title' => $this->t('Asset source URL'),
      '#prefix' => '<div class="dalim_main_field">',
      '#type' => 'textfield',
      '#attributes' => [
        'class' => ['dalim_es_url'],
      ],
      '#default_value' => $items[$delta]->url ?? NULL,
      '#description' => $this->t('Permanent link to the asset. Local assets start with public://'),
    ];
    $element['open-browser'] = [
      '#title' => $this->t('Select asset'),
      '#type' => 'markup',
      '#suffix' => '</div>',
      '#markup' => Link::fromTextAndUrl($this->t('Select asset'), $link_url)->toString(),
      '#attached' => ['library' => ['core/drupal.dialog.ajax']],
    ];
    $element['preview'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#prefix' => '<div class="dalim_body_field"><div class="dalim_preview_wrapper_field">',
      '#suffix' => '</div>',
      '#attributes' => [
        'class' => 'dalim_preview_img',
        'id' => strtolower($name) . '-preview-img-' . $delta,
        'style' => [
          'background-image: url("' . $items[$delta]->thumbnail . '")',
        ],
      ],
    ];
    $element['width'] = [
      '#title' => $this->t('Asset width'),
      '#type' => 'textfield',
      '#prefix' => '<div class="dalim_fields_wrapper_field"><div class="dalim_line_field"><div class="dalim_left_field">',
      '#suffix' => '</div>',
      '#default_value' => $items[$delta]->width ?? '500px',
      '#description' => $this->t('100px, 30%, auto ...'),
    ];
    $element['height'] = [
      '#title' => $this->t('Asset height'),
      '#type' => 'textfield',
      '#prefix' => '<div class="dalim_right_field">',
      '#suffix' => '</div></div>',
      '#default_value' => $items[$delta]->height ?? 'auto',
      '#description' => $this->t('100px, 30%, auto ...'),
    ];
    $element['alt'] = [
      '#title' => $this->t('Alt text'),
      '#prefix' => '<div class="dalim_line_field"><div class="dalim_left_field">',
      '#suffix' => '</div>',
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->alt ?? NULL,
      '#description' => $this->t('Alt. text for images. Used as name for documents.'),
    ];
    $element['hyperlink'] = [
      '#title' => $this->t('Hyperlink (For image only)'),
      '#type' => 'textfield',
      '#prefix' => '<div class="dalim_right_field">',
      '#suffix' => '</div></div></div></div>',
      '#default_value' => $items[$delta]->hyperlink ?? NULL,
      '#description' => $this->t('Automatic. Change only if wanted.'),
    ];
    $element['type'] = [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->type ?? NULL,
      '#attributes' => [
        'style' => 'display: none;',
      ],
    ];
    $element['thumbnail'] = [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->thumbnail ?? NULL,
      '#maxlength' => 256,
      '#attributes' => [
        'style' => 'display: none;',
      ],
    ];

    return $element;
  }

}
