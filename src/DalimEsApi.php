<?php

namespace Drupal\dalim_es;

use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;

define("DALIM_ESAPI_URL", "DALIM_ESAPI_URL");
define("DALIM_ESAPI_AUTO_LOGOUT", "DALIM_ESAPI_AUTO_LOGOUT");
define("DALIM_ESAPI_SESSION_ID", "DALIM_ESAPI_SESSION_ID");
define("DALIM_ESAPI_HTTP_HEADER", "DALIM_ESAPI_HTTP_HEADER");

/**
 * DALIM ES API service.
 *
 * @package Drupal\dalim_es
 */
class DalimEsApi {

  /**
   * DALIM ES API configuration.
   *
   * @var array
   */
  private $dalimEsConfig;

  /**
   * DALIM ES server.
   *
   * @var string
   */
  private $url = NULL;

  /**
   * DALIM ES user.
   *
   * @var string
   */
  private $username = NULL;

  /**
   * DALIM ES password.
   *
   * @var string
   */
  private $password = NULL;

  /**
   * DALIM ES options.
   *
   * @var array
   */
  private $options = [
    DALIM_ESAPI_URL => NULL,
    DALIM_ESAPI_AUTO_LOGOUT => TRUE,
    DALIM_ESAPI_HTTP_HEADER => [],
    DALIM_ESAPI_SESSION_ID => 'PHP',
  ];

  /**
   * Id for API call.
   *
   * @var string
   */
  private $nextid = 1;

  /**
   * Date of last check.
   *
   * @var string
   */
  private $lastcheck = 0;

  /**
   * Last error.
   *
   * @var string
   */
  private $lasterr = NULL;

  /**
   * PHP session.
   *
   * @var string
   */
  private $phpsession = FALSE;

  /**
   * Last header.
   *
   * @var string
   */
  private $lastheader = NULL;

  /**
   * HTTP client object.
   *
   * @var GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('http_client'),
      );
  }

  /**
   * Construct a DalimEsApi object.
   *
   * @param GuzzleHttp\ClientInterface $httpClient
   *   HTTP client service.
   */
  public function __construct(ClientInterface $httpClient) {
    $this->httpClient = $httpClient;
  }

  /**
   * Get the configuration.
   *
   * @param array $config
   *   Arguments to configure the object.
   */
  public function dalimEsConfiguration(array $config) {
    $this->dalimEsConfig = $config;

    $this->url = $config['server_name'] . '/Esprit/public/Interface/RPC';
    $this->username = $config['admin_username'];
    $this->password = $config['admin_password'];
    $this->options[DALIM_ESAPI_URL] = $this->url;
  }

  /**
   * Log in with the API.
   *
   * @return array
   *   ES API Session ID
   */
  public function login() {
    $username = $this->username;
    $password = $this->password;

    $result = $this->call(
    "admin.login",
    ["user" => $username, "password" => $password]
    );
    if ($result == FALSE) {
      return FALSE;
    }
    $this->lastHeader();

    if ($result["status"] == "loggedIn") {
      if (isset($result["sessionID"])) {
        $this->options[DALIM_ESAPI_SESSION_ID] = $result["sessionID"];
      }
      else {
        $headers = $this->lastHeader();
        foreach ($headers as $header) {
          $matches = [];
          if (preg_match('/^SET-COOKIE: JSESSIONID=([0-9A-F]+)(;.*|$)/', strtoupper($header), $matches)) {
            $this->options[DALIM_ESAPI_SESSION_ID] = $matches[1];
            break;
          }
          elseif (preg_match('/^COOKIE: JSESSIONID=([0-9A-F]+)(;.*|$)/', strtoupper($header), $matches)) {
            $this->options[DALIM_ESAPI_SESSION_ID] = $matches[1];
          }
        }
      }
      if ($this->phpsession) {
        $_SESSION[DALIM_ESAPI_SESSION_ID] = $this->options[DALIM_ESAPI_SESSION_ID];
      }
      return $this->options[DALIM_ESAPI_SESSION_ID];
    }
    else {
      $this->options[DALIM_ESAPI_SESSION_ID] = NULL;
      // @todo ... last error.
      return FALSE;
    }
  }

  /**
   * Log out of the API.
   *
   * @return bool
   *   Success or fail
   */
  public function logout() {
    $result = $this->call("admin.logout");
    if (is_array($result) && $result["status"] == "loggedOut") {
      $this->options[DALIM_ESAPI_SESSION_ID] = NULL;
      return TRUE;
    }
    else {
      // @todo ... last error.
      return FALSE;
    }
  }

  /**
   * Get the last error.
   *
   * @return array
   *   The last error
   */
  public function getError() {
    if (isset($this->lasterr["error"])) {
      return $this->lasterr["message"];
    }
    elseif (isset($this->lasterr["message"])) {
      return $this->lasterr["message"];
    }
    else {
      return $this->lasterr;
    }
  }

  /**
   * Get the last header.
   *
   * @return array
   *   The last header
   */
  public function lastHeader() {
    if ($this->lastheader == NULL) {
      return NULL;
    }

    if (is_string($this->lastheader)) {
      $headers = explode("\n", $this->lastheader);
      $this->lastheader = [];
      foreach ($headers as $header) {
        $this->lastheader[] = trim($header);
      }
    }

    return $this->lastheader;
  }

  /**
   * Check if we're logged in.
   *
   * @return array
   *   The session ID
   */
  public function isLogged() {
    if ($this->options[DALIM_ESAPI_SESSION_ID] != NULL) {
      if (time() > $this->lastcheck + 1) {
        $this->lastcheck = time();

        $result = $this->curlJsonPost([
          'ID' => '1',
          'method' => 'admin.getVersion',
        ]);
        if ($result == FALSE) {
          $this->options[DALIM_ESAPI_SESSION_ID] = NULL;
        }
      }
    }
    return $this->options[DALIM_ESAPI_SESSION_ID] != NULL;
  }

  /**
   * Get the metadatas of an asset.
   *
   * @param string $id
   *   Id of the asset.
   *
   * @return array
   *   The metadatas
   */
  public function getObjectMetadatas($id) {
    $result = $this->call("metadata.getObjectMetadata", [
      "ID" => $id,
      "class" => "PageOrder",
    ]);
    return $result['metadatas'];
    // Return $result;.
  }

  /**
   * Get the approval status of an asset.
   *
   * @param string $id
   *   Id of the asset.
   *
   * @return array
   *   API result
   */
  public function getApprovalStatus($id) {
    $result = $this->call("document.approvalStatus", ["ID" => $id]);
    return $result;
  }

  /**
   * Get an asset.
   *
   * @param string $id
   *   Id of the asset.
   *
   * @return array
   *   API result
   */
  public function getDocument($id) {
    $result = $this->call("document.get", ["ID" => $id]);
    return $result;
  }

  /**
   * Get a share link of an asset.
   *
   * @param string $class
   *   Class of document.
   * @param string $sharingId
   *   Id of the asset.
   * @param string $type
   *   Type of the asset.
   * @param string $viewer
   *   Web user name.
   *
   * @return string
   *   Share link
   */
  public function getSharingLink($class, $sharingId, $type, $viewer = NULL) {
    if ($viewer == NULL) {
      $result = $this->call(
      "production.shareObject",
      ["class" => $class, "ID" => $sharingId, "type" => $type]
      );
    }
    else {
      $result = $this->call(
      "production.shareObject",
      [
        "class" => $class,
        "ID" => $sharingId,
        "type" => $type,
        "viewAs" => $viewer,
      ]
      );
    }
    if (isset($result['sharingKey'])) {
      $sharingKey = $result['sharingKey'];
      return $sharingKey;
    }
  }

  /**
   * Search assets in DALIM ES with a query.
   *
   * @param string $query
   *   Name of the user.
   *
   * @return array
   *   List of assets
   */
  public function getProductionSearch($query) {
    $result = $this->call("production.search", ["query" => $query]);
    return $result['objectList'];
  }

  /**
   * Get a user in DALIM ES.
   *
   * @param string $name
   *   Name of the user.
   *
   * @return array
   *   API response
   */
  public function getUser($name) {
    $result = $this->call("directory.getUser", ["ID" => $name]);
    return $result;
  }

  /**
   * Prepare the request to the ES API.
   *
   * @param string $method
   *   The method of the request.
   * @param array $params
   *   The parameters of the request.
   *
   * @return array
   *   API response
   */
  public function call($method, array $params = NULL) {
    // $this->_auto_login();
    $request = ["id" => ($this->nextid++), "method" => $method];
    if ($params != NULL) {
      $request["params"] = $params;
    }

    $fullRequest = [
    ["id" => 1, "method" => "admin.login"],
      $request,
    ["id" => NULL, "method" => "admin.logout"],
    ];
    $response = $this->curlJsonPost($fullRequest);
    if ($response == FALSE) {
      return FALSE;
    }

    if (!is_array($response)) {
      $this->lasterr = ["code" => -1, "message" => "Invalid response"];
      return FALSE;
    }

    if (isset($response["error"])) {
      $this->lasterr = $response["error"];
      return FALSE;
    }

    if (isset($response["result"])) {
      return $response["result"];
    }

    return $response;
  }

  /**
   * Send the request to the ES API.
   *
   * @param array $request
   *   The request to send to the API.
   *
   * @return array
   *   API response
   */
  private function curlJsonPost(array $request) {
    $headers = [
      "Content-Type: application/json; charset=UTF-8",
      "User-Agent: PHP/" . phpversion(),
    ];
    if ($this->options[DALIM_ESAPI_SESSION_ID] != NULL) {
      $headers[] = "Cookie: JSESSIONID=" . $this->options[DALIM_ESAPI_SESSION_ID];
    }

    $headers = array_merge($this->options[DALIM_ESAPI_HTTP_HEADER], $headers);
    $url     = $this->options[DALIM_ESAPI_URL];

    if ($url == NULL) {
      trigger_error('Call GRN', E_USER_ERROR);
      /*
      $module = Application::getModule('es');
      $config = $module->config;
      $url    = $config['es_url'] . '/public/Interface/RPC';
       */
    }

    try {
      $httpRequest = $this->httpClient->post($url, [
        'auth' => [$this->username, $this->password],
        'body' => json_encode($request),
        'headers' => $headers,
      ]);
      $response = $httpRequest->getBody();
      $this->lastheader = $httpRequest->getHeaders();

    }
    catch (RequestException $e) {
      watchdog_exception('dalim_es', $e->getMessage());
    }

    $httpcode = $httpRequest->getStatusCode();
    if ($httpcode == 200) {
      $response = json_decode($response, TRUE);

      if (isset($response[1]["error"])) {
        $this->lasterr = $response[1]["error"];
        return FALSE;
      }

      return $response[1];
    }
    else {
      $this->lasterr = [
        "code" => $httpcode,
        "message" => "HTTP Status " . $httpcode,
      ];
      return FALSE;
    }
  }

}
