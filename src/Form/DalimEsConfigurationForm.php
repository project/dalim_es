<?php

namespace Drupal\dalim_es\Form;

use Drupal\dalim_es\DalimEsCmisApi;
use Drupal\dalim_es\DalimEsApi;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure DALIM ES API.
 *
 * @package Drupal\dalim_es\Form
 */
class DalimEsConfigurationForm extends ConfigFormBase {

  /**
   * DALIM ES Api service.
   *
   * @var \Drupal\dalim_es\DalimEsApi
   */
  protected $dalimEsApi;

  /**
   * DALIM ES CMIS Api service.
   *
   * @var \Drupal\dalim_es\DalimEsCmisApi
   */
  protected $dalimEsCmisApi;

  /**
   * Renderer object.
   *
   * @var Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a DalimEsConfigurationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param Drupal\dalim_es\DalimEsCmisApi $dalimEsCmisApi
   *   DALIM ES CMIS service.
   * @param Drupal\dalim_es\DalimEsApi $dalimEsApi
   *   DALIM ES service.
   * @param Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, DalimEsCmisApi $dalimEsCmisApi, DalimEsApi $dalimEsApi, Renderer $renderer) {
    parent::__construct($config_factory);
    $this->dalimEsApi = $dalimEsApi;
    $this->dalimEsCmisApi = $dalimEsCmisApi;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory'),
          $container->get('dalim_es_cmis_api'),
          $container->get('dalim_es_api'),
          $container->get('renderer'),
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dalim_es_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['dalim_es.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dalim_es.settings');

    $link_url = Url::fromRoute('dalim_es.updateAssets');
    $link_url->setOptions([
      'attributes' => [
        'class' => ['use-ajax', 'button', 'button--small'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 1000]),
      ],
    ]);

    $form['server_name'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Server name'),
      '#default_value' => $config->get('server_name'),
      '#description' => $this->t('The name of your ES Server'),
    ];
    $form['admin_username'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Admin username'),
      '#default_value' => $config->get('admin_username'),
      '#description' => $this->t('The username of your DALIM ES account'),
    ];
    $form['admin_password'] = [
      '#required' => TRUE,
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get('admin_password'),
      '#description' => $this->t('The password of your DALIM ES account'),
    ];
    $form['api_mode'] = [
      '#required' => TRUE,
      '#type' => 'select',
      '#title' => $this->t('API mode'),
      '#default_value' => $config->get('api_mode'),
      '#description' => $this->t('ES API needs the API licence, it allows dynamic links with assets (revision-aware), full-text search, dialog links and asset download links.'),
      '#options' => [
        'dalim_es_cmis_api' => $this->t('CMIS API'),
        'dalim_es_es_api' => $this->t('ES API'),
      ],
      '#default_value' => $config->get('api_mode'),
    ];
    $form['web_client'] = [
      '#required' => FALSE,
      '#type' => 'textfield',
      '#title' => $this->t('Web client username (Optional)'),
      '#default_value' => $config->get('web_client'),
      '#description' => $this->t('The DALIM ES user account used to restrict DIALOGUE rights.'),
    ];
    $form['test'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API connection test'),
      'check' => [
        '#type' => 'button',
        '#limit_validation_errors' => [],
        '#value' => $this->t('Test connection'),
        '#ajax' => ['callback' => '::testConnectionAjaxCallback'],
      ],
      'wrapper' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'id' => 'dalim-connection-test',
        ],
      ],
    ];
    $form['update_assets'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Update your local assets'),
      'wrapper' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
      ],
      'update' => [
        '#title' => $this->t('Update your assets'),
        '#type' => 'markup',
        '#suffix' => '</div>',
        '#markup' => Link::fromTextAndUrl($this->t('Update'), $link_url)->toString(),
        '#attached' => ['library' => ['core/drupal.dialog.ajax']],
      ],
      '#description' => $this->t('The more imported files you have, the more time it will take.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Reset SESSIONS to avoid unreachable folders
    // if the server change.
    $_SESSION['DALIM_ES_POST_ESFOLDER'] = '4:0:1';
    $_SESSION['DALIM_ES_POST_ESNAME'] = '';
    $_SESSION['DALIM_ES_POST_PREV_ID'] = '';
    $_SESSION['DALIM_ES_POST_POSTNAME'] = '';

    $this->config('dalim_es.settings')
      ->set('server_name', $form_state->getValue('server_name'))
      ->set('admin_username', $form_state->getValue('admin_username'))
      ->set('admin_password', $form_state->getValue('admin_password'))
      ->set('api_mode', $form_state->getValue('api_mode'))
      ->set('web_client', $form_state->getValue('web_client'))
      ->save();

  }

  /**
   * AJAX callback for test connection button.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Result markup
   */
  public function testConnectionAjaxCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $return_markup = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => ['id' => 'dalim-connection-test'],
    ];

    $server = $form_state->getValue('server_name');
    $user = $form_state->getValue('admin_username');
    $password = $form_state->getValue('admin_password');
    $webuser = $form_state->getValue('web_client');
    $api = $form_state->getValue('api_mode');

    if ($this->testApiConnection($server, $user, $password, $webuser, $api)) {
      $return_markup['#value'] = 'The API connection was established successfully.';
      $return_markup['#attributes']['style'] = 'color: green;';
      if ($api == 'dalim_es_es_api') {
        $this->dalimEsApi->logout();
      }
    }
    else {
      $return_markup['#value'] = 'Could not establish connection with DALIM ES.';
      $return_markup['#attributes']['style'] = 'color: red;';
    }

    $response->addCommand(new ReplaceCommand('#dalim-connection-test', $this->renderer->render($return_markup)));
    return $response;
  }

  /**
   * Test the connection to the DALIM ES server.
   *
   * @param string $server
   *   The DALIM ES server.
   * @param string $user
   *   The username.
   * @param string $password
   *   The password.
   * @param string $webuser
   *   The web username.
   * @param string $api
   *   The API used.
   *
   * @return bool
   *   Can connect or not
   */
  protected function testApiConnection($server, $user, $password, $webuser, $api) {
    try {
      $this->dalimEsCmisApi->dalimCmisConfiguration([
        "DALIM_CMIS_API_AUTO_LOGOUT" => FALSE,
        "DALIM_CMIS_API_URL" => $server . "/Esprit/browser",
        "DALIM_CMIS_API_SERVER" => $server,
      ]);
      if ($this->dalimEsCmisApi->login($user, $password)) {
        if ($api == 'dalim_es_es_api') {
          $this->dalimEsApi->DalimEsConfiguration([
            'server_name' => $server,
            'admin_username' => $user,
            'admin_password' => $password,
          ]);
          if ($this->dalimEsApi->login()) {
            if ($webuser != '') {
              return $this->dalimEsApi->getUser($webuser);
            }
            else {
              return TRUE;
            }

          }
          else {
            return FALSE;
          }
        }
        else {
          return TRUE;
        }
      }
      else {
        return FALSE;
      }

    }
    catch (\Exception $exception) {
      return FALSE;
    }
    return TRUE;
  }

}
