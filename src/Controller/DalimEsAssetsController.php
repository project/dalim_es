<?php

namespace Drupal\dalim_es\Controller;

use Drupal\media\Entity\Media;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\dalim_es\DalimEsApi;
use Drupal\dalim_es\DalimEsCmisApi;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\file\FileRepository;
use Drupal\Core\File\FileSystem;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Create share link / Upload assets from DALIM ES.
 */
class DalimEsAssetsController extends ControllerBase {

  /**
   * Renderer object.
   *
   * @var Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * File repository object.
   *
   * @var Drupal\file\FileRepository
   */
  protected $fileRepository;

  /**
   * File repository object.
   *
   * @var Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Request stack object.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $stack;

  /**
   * HTTP client object.
   *
   * @var GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Entity type manager object.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * DALIM ES CMIS API object.
   *
   * @var Drupal\dalim_es\DalimEsCmisApi
   */
  protected $dalimEsCmisApi;

  /**
   * DALIM ES API object.
   *
   * @var Drupal\dalim_es\DalimEsApi
   */
  protected $dalimEsApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('renderer'),
          $container->get('file.repository'),
          $container->get('file_system'),
          $container->get('request_stack'),
          $container->get('http_client'),
          $container->get('entity_type.manager'),
          $container->get('dalim_es_cmis_api'),
          $container->get('dalim_es_api'),
      );
  }

  /**
   * Construct a DalimEsAssetsController object.
   *
   * @param Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   * @param Drupal\file\FileRepository $fileRepository
   *   File repository service.
   * @param Drupal\Core\File\FileSystem $fileSystem
   *   File system service.
   * @param Symfony\Component\HttpFoundation\RequestStack $stack
   *   Request stack service.
   * @param GuzzleHttp\ClientInterface $httpClient
   *   Http client service.
   * @param Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Entity type manager service.
   * @param Drupal\dalim_es\DalimEsCmisApi $dalimEsCmisApi
   *   DALIM ES CMIS service.
   * @param Drupal\dalim_es\DalimEsApi $dalimEsApi
   *   DALIM ES service.
   */
  public function __construct(Renderer $renderer, FileRepository $fileRepository, FileSystem $fileSystem, RequestStack $stack, ClientInterface $httpClient, EntityTypeManager $entityTypeManager, DalimEsCmisApi $dalimEsCmisApi, DalimEsApi $dalimEsApi) {
    $this->renderer = $renderer;
    $this->fileRepository = $fileRepository;
    $this->fileSystem = $fileSystem;
    $this->stack = $stack;
    $this->httpClient = $httpClient;
    $this->entityTypeManager = $entityTypeManager;
    $this->dalimEsCmisApi = $dalimEsCmisApi;
    $this->dalimEsApi = $dalimEsApi;
  }

  /**
   * Ajax callback that create the shared link or upload the asset in Drupal.
   *
   * @param int $objectId
   *   The objectId of the asset.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Asset link
   */
  public function getSharingLink($objectId) {
    $config = $this->config('dalim_es.settings');

    $server = $config->get('server_name');
    $user = $config->get('admin_username');
    $password = $config->get('admin_password');
    $api = $config->get('api_mode');
    $webuser = $config->get('web_client');

    $type = $this->stack->getCurrentRequest()->query->get('linkType');
    $method = $this->stack->getCurrentRequest()->query->get('method');
    $url = $this->stack->getCurrentRequest()->query->get('url');
    $name = $this->stack->getCurrentRequest()->query->get('name');
    $assetType = $this->stack->getCurrentRequest()->query->get('assetType');

    $link = [];
    $link['thumbnail'] = $url;

    if ($api === 'dalim_es_es_api') {
      $this->dalimEsApi->dalimEsConfiguration([
        'server_name' => $server,
        'admin_username' => $user,
        'admin_password' => $password,
      ]);

      $path = $server . '/Esprit/shared/';
      // Sharing link of the asset.
      if ($type == 'no_link') {
        $result = $this->dalimEsApi->getSharingLink('PageOrder', $objectId, 'Download', $webuser);
        // Return the sharking key in the good URL.
        $link['download'] = $path . $result;
        $link['link'] = '';
      }
      // Sharing link of the asset + download link.
      elseif ($type == 'dl_link') {
        $result = $this->dalimEsApi->getSharingLink('PageOrder', $objectId, 'Download', $webuser);

        // Return the sharking key in the good URL.
        $link['download'] = $path . $result;
        $link['link'] = $path . $result;
      }
      // Virtual book link.
      elseif ($type == 'virtual_book') {
        $result = $this->dalimEsApi->getSharingLink('PageOrder', $objectId, 'Download', $webuser);

        // Return the sharking key in the good URL.
        $link['download'] = $path . $result;
        $link['link'] = $path . $result;
      }
      // Sharing link of the asset + view link.
      else {
        $result1 = $this->dalimEsApi->getSharingLink('PageOrder', $objectId, 'Download', $webuser);
        $result2 = $this->dalimEsApi->getSharingLink('PageOrder', $objectId, 'View', $webuser);

        // Return the sharing key in the good URL.
        $link['download'] = $path . $result1;
        $link['link'] = $path . $result2;
      }
    }
    else {
      $link['link'] = '';

      if ($method == 'no_dl') {
        $link['download'] = $url;
      }
    }

    if ($method != 'no_dl') {
      // If we don't want the thumbnail,
      // we cut the string to take off the streamId.
      if ($method == 'original' || str_starts_with($assetType, 'video')) {
        $str = explode('&', $url);
        $url = $str[0] . '&' . $str[2];
      }

      $cmisId = explode("&", $url);
      $cmisId = explode("=", $cmisId[0]);
      $cmisId = explode(":", $cmisId[1]);
      $cmisId = $cmisId[1];

      $finalName = substr($method, 0, 3) . $cmisId . '-' . $name;

      // Need to change the extension to create the thumbnail.
      if ($method == 'thumbnail') {
        $finalName = substr($finalName, 0, strlen($finalName) - 3) . 'jpg';
        $name = substr($name, 0, strlen($name) - 3) . 'jpg';
        $assetType = 'image';
      }

      $request = $this->httpClient->get($url);
      $response = $request->getBody();

      $directory = 'public://dalim_es_assets/';
      $uploadfile = $directory . $finalName;

      // Create the directory if it's not.
      $this->fileSystem->prepareDirectory(
            $directory,
            FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
        );

      // Create the file.
      $file = $this->fileRepository->writedata(
            $response,
            $uploadfile,
            FileSystemInterface::EXISTS_REPLACE
        );

      // Create the associated media.
      if (str_starts_with($assetType, 'image') || $method == 'thumbnail') {
        $media = Media::create([
          'bundle' => 'image',
          'uid' => $this->currentUser()->id(),
          'field_media_image' => [
            'target_id' => $file->id(),
            'alt' => $name,
          ],
        ]);
      }
      elseif (str_starts_with($assetType, 'video')) {
        $media = Media::create([
          'bundle' => 'video',
          'uid' => $this->currentUser()->id(),
          'field_media_video' => [
            'target_id' => $file->id(),
          ],
        ]);
      }
      else {
        $media = Media::create([
          'bundle' => 'document',
          'uid' => $this->currentUser()->id(),
          'field_media_document' => [
            'target_id' => $file->id(),
          ],
        ]);
      }
      $media
        ->setName($name)
        ->setPublished(TRUE)
        ->save();

      $link['download'] = $uploadfile;

    }

    $assetType = explode("/", $assetType)[0];

    $link['name'] = $name;
    $link['type'] = $assetType;

    return new JsonResponse(['status' => 0, 'data' => $link]);
  }

  /**
   * Ajax callback that update the assets from DALIM ES.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Assets updated
   */
  public function updateAssets() {
    $response = new AjaxResponse();

    $options = [
      'dialogClass' => 'popup-dialog-class',
      'width' => '60%',
      'height' => '60%',
    ];

    $config = $this->config('dalim_es.settings');

    $server = $config->get('server_name');
    $user = $config->get('admin_username');
    $password = $config->get('admin_password');

    $directory = 'public://dalim_es_assets/';
    $files = $this->fileSystem->scanDirectory($directory, '/.*/');

    $modal_page['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $this->t('<h3>The following files have been updated:</h3>'),
      'files' => [
        '#type' => 'html_tag',
        '#tag' => 'ul',
      ],
    ];

    foreach ($files as $file) {
      $fileName = $file->filename;
      $uri = $file->uri;

      // Explode the file name to separate the asset name from the type and ID.
      $separation = explode('-', $fileName);
      $prefix = $separation[0];
      array_shift($separation);
      $esFilename = implode('-', $separation);

      $url = $server . "/Esprit/browser/4:0:1";
      $body = [
        "cmisaction" => "query",
        "statement" => "select * from cmis:document where cmis:name = '" . $esFilename . "'",
      ];

      try {
        $request = $this->httpClient->post($url, [
          'auth' => [$user, $password],
          'form_params' => $body,
        ]);
        $clientResponse = $request->getBody();
      }
      catch (RequestException $e) {
        watchdog_exception('dalim_es', $e->getMessage());
      }

      $result = json_decode($clientResponse, TRUE);

      foreach ($result['results'] as $esFile) {
        // Adding configuration for the API class.
        $this->dalimEsCmisApi->dalimCmisConfiguration([
          "DALIM_CMIS_API_AUTO_LOGOUT" => FALSE,
          "DALIM_CMIS_API_URL" => $server . "/Esprit/browser",
          "DALIM_CMIS_API_SERVER" => $server,
        ]);

        // Telling the API when you are logged in or out.
        if ($_SESSION["login"] === "true") {
          $this->dalimEsCmisApi->isLogged();
        }
        else {
          $this->dalimEsCmisApi->logout();
        }

        $this->dalimEsCmisApi->login($user, $password);
        $obj = $esFile['properties']["cmis:objectId"]["value"];

        if (substr($obj, 2, -2) == substr($prefix, 3) && $obj != '') {
          $obj = substr($obj, 0, -1) . "0";

          $cmisUrl = $this->dalimEsCmisApi->getThumbnail($obj);
          // $type = $file['properties']["cmis:contentStreamMimeType"]["value"];
          $files = $this->entityTypeManager
            ->getStorage('file')
            ->loadByProperties(['uri' => $uri]);
          $localFile = reset($files) ?: NULL;
          $type = $localFile->getMimeType();

          $download = substr($prefix, 0, 3);

          // Take off the streamID of the url
          // to have the original and not the thumbnail.
          if ($download == 'ori' || str_starts_with($type, 'video')) {
            $str = explode('&', $cmisUrl);
            $cmisUrl = $str[0] . '&' . $str[2];
          }
          $uploadfile = $uri;

          $httpRequest = $this->httpClient->get($cmisUrl);
          $httpResponse = $httpRequest->getBody();

          // Update the file in repository.
          $file = $this->fileRepository->writedata(
          $httpResponse,
          $uploadfile,
          FileSystemInterface::EXISTS_REPLACE
            );

          $modal_page['files'][$fileName] = [
            '#type' => 'html_tag',
            '#tag' => 'li',
            '#value' => $fileName,
          ];
        }
      }
    }
    $response->addCommand(
          new OpenModalDialogCommand($this->t('Updated files'),
          $this->renderer->render($modal_page), $options),
      );
    return $response;
  }

}
