<?php

namespace Drupal\dalim_es\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dalim_es\DalimEsApi;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Display the metadatas from DALIM ES.
 */
class DalimEsMetadatasController extends ControllerBase {

  /**
   * DALIM ES API object.
   *
   * @var Drupal\dalim_es\DalimEsApi
   */
  protected $dalimEsApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('dalim_es_api'),
      );
  }

  /**
   * Construct a DalimEsAssetsController object.
   *
   * @param Drupal\dalim_es\DalimEsApi $dalimEsApi
   *   DALIM ES service.
   */
  public function __construct(DalimEsApi $dalimEsApi) {
    $this->dalimEsApi = $dalimEsApi;
  }

  /**
   * Ajax callback that display the metadatas for the current asset.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Metadatas
   */
  public function getMetadatas($objectId) {
    $config = $this->config('dalim_es.settings');

    $server = $config->get('server_name');
    $user = $config->get('admin_username');
    $password = $config->get('admin_password');

    // Creating a new object for the API class.
    $this->dalimEsApi->dalimEsConfiguration([
      'server_name' => $server,
      'admin_username' => $user,
      'admin_password' => $password,
    ]);

    // Call the API to get the metadatas.
    $result = $this->dalimEsApi->getObjectMetadatas($objectId);
    $status = $this->dalimEsApi->getApprovalStatus($objectId);
    $doc = $this->dalimEsApi->getDocument($objectId);

    // Select the interesting metadatas.
    $usefullMetadatas = [
      'fileSize',
      'height',
      'width',
      'CreateDate',
      'mimeCategory',
      'rights',
    ];
    $metadatas = [];

    foreach ($result as $object) {
      if (in_array($object[1], $usefullMetadatas)) {
        $metadatas[$object[1]] = $object[2];
      }
    }

    $metadatas['status'] = $status['status'];
    $metadatas['creationDate'] = $doc['creationDate'];
    $metadatas['lastModificationDate'] = $doc['lastModificationDate'];
    $metadatas['lastModificationUser'] = $doc['lastModificationUser'];
    $metadatas['currentRevision'] = $doc['currentRevision'];
    $metadatas['lastRevision'] = $doc['lastRevision'];

    return new JsonResponse(['status' => 0, 'data' => $metadatas]);
  }

}
