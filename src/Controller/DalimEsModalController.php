<?php

namespace Drupal\dalim_es\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\dalim_es\DalimEsCmisApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Renderer;

/**
 * Display the modal that browse DALIM ES.
 */
class DalimEsModalController extends ControllerBase {

  /**
   * Renderer object.
   *
   * @var Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * DALIM ES CMIS API object.
   *
   * @var Drupal\dalim_es\DalimEsCmisApi
   */
  protected $dalimEsCmisApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('renderer'),
          $container->get('dalim_es_cmis_api'),
      );
  }

  /**
   * Construct a DalimEsModalController object.
   *
   * @param Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   * @param Drupal\dalim_es\DalimEsCmisApi $dalimEsCmisApi
   *   DALIM ES CMIS service.
   */
  public function __construct(Renderer $renderer, DalimEsCmisApi $dalimEsCmisApi) {
    $this->renderer = $renderer;
    $this->dalimEsCmisApi = $dalimEsCmisApi;
  }

  /**
   * Ajax callback that open the modal.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Modal and render array
   */
  public function openModal() {
    $response = new AjaxResponse();

    $options = [
      'dialogClass' => [
        'popup-dialog-class',
        'dalim-modal',
      ],
      'width' => '80%',
      'height' => '80%',
    ];

    $config = $this->config('dalim_es.settings');

    $server = $config->get('server_name');
    $user = $config->get('admin_username');
    $password = $config->get('admin_password');
    $api_used = $config->get('api_mode');

    // Adding configuration for the API class.
    $this->dalimEsCmisApi->dalimCmisConfiguration([
      "DALIM_CMIS_API_AUTO_LOGOUT" => FALSE,
      "DALIM_CMIS_API_URL" => $server . "/Esprit/browser",
      "DALIM_CMIS_API_SERVER" => $server,
    ]);

    // Telling the API when you are logged in or out.
    if ($_SESSION["login"] === "true") {
      $this->dalimEsCmisApi->isLogged();
    }
    else {
      $this->dalimEsCmisApi->logout();
    }

    if ($this->dalimEsCmisApi->login($user, $password)) {
      $modal_page = $this->dalimEsCmisApi->displayMenu($api_used);
      $response->addCommand(
            new OpenModalDialogCommand($this->t('DALIM ES'),
            $this->renderer->render($modal_page), $options),
        );
    }

    return $response;
  }

}
