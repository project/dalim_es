<?php

namespace Drupal\dalim_es\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dalim_es\DalimEsCmisApi;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Renderer;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Display the browsing in DALIM ES.
 */
class DalimEsFoldersController extends ControllerBase {

  /**
   * Renderer object.
   *
   * @var Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Request stack object.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $stack;

  /**
   * DALIM ES CMIS API object.
   *
   * @var Drupal\dalim_es\DalimEsCmisApi
   */
  protected $dalimEsCmisApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('renderer'),
          $container->get('request_stack'),
          $container->get('dalim_es_cmis_api'),
      );
  }

  /**
   * Construct a DalimEsFoldersController object.
   *
   * @param Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   * @param Symfony\Component\HttpFoundation\RequestStack $stack
   *   Request stack service.
   * @param Drupal\dalim_es\DalimEsCmisApi $dalimEsCmisApi
   *   DALIM ES CMIS service.
   */
  public function __construct(Renderer $renderer, RequestStack $stack, DalimEsCmisApi $dalimEsCmisApi) {
    $this->renderer = $renderer;
    $this->stack = $stack;
    $this->dalimEsCmisApi = $dalimEsCmisApi;
  }

  /**
   * Ajax callback that display the current folder or the result of the search.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   DALIM ES interface
   */
  public function getFolder($esfolder) {
    $config = $this->config('dalim_es.settings');

    $server = $config->get('server_name');
    $user = $config->get('admin_username');
    $password = $config->get('admin_password');
    $api_used = $config->get('api_mode');

    $esname = $this->stack->getCurrentRequest()->query->get('esname');
    $prevId = $this->stack->getCurrentRequest()->query->get('prevId');

    // Adding configuration for the API class.
    $this->dalimEsCmisApi->dalimCmisConfiguration([
      "DALIM_CMIS_API_AUTO_LOGOUT" => FALSE,
      "DALIM_CMIS_API_URL" => $server . "/Esprit/browser",
      "DALIM_CMIS_API_SERVER" => $server,
    ]);

    // Telling the API when you are logged in or out.
    if ($_SESSION["login"] === "true") {
      $this->dalimEsCmisApi->isLogged();
    }
    else {
      $this->dalimEsCmisApi->logout();
    }
    if ($this->dalimEsCmisApi->login($user, $password)) {
      if (NULL != $this->stack->getCurrentRequest()->query->get('global_search')) {
        $_SESSION['DALIM_ES_POST_POSTNAME'] = $this->stack->getCurrentRequest()->query->get('global_search');
      }
      else {
        $_SESSION['DALIM_ES_POST_POSTNAME'] = NULL;
      }
      $_SESSION['DALIM_ES_POST_ESFOLDER'] = $esfolder;
      $_SESSION['DALIM_ES_POST_ESNAME'] = $esname;
      $_SESSION['DALIM_ES_POST_PREV_ID'] = $prevId;

      $response = $this->dalimEsCmisApi->displayMenu($api_used);
    }
    return new JsonResponse([
      'status' => 0,
      'data' => $this->renderer->render($response),
    ]);
  }

}
